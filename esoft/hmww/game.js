class GameField {
    state = [
        [null, null, null],
        [null, null, null],
        [null, null, null],
    ];
    mode = 'x';
    isOverGame = false;

    getGameFieldStatus() {
        const gameFieldFlattened = this.state.flat();

        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
        ];

        const winnersSet = new Set();

        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (
                gameFieldFlattened[a] &&
                gameFieldFlattened[a] === gameFieldFlattened[b] &&
                gameFieldFlattened[a] === gameFieldFlattened[c]
            ) {
                winnersSet.add(gameFieldFlattened[a]);
            }
        }

        if (winnersSet.size === 0) {
            if (gameFieldFlattened.includes(null)) return 'Игра ещё не окончена';
            else return 'Ничья';
        } else if (winnersSet.size === 1) {
            return winnersSet.has('x') ? 'Крестики победили' : 'Нолики победили';
        } else if (winnersSet.size === 2) {
            return 'Ничья';
        }
    }
    setMode() {
        this.mode = this.mode === 'x' ? 'o' : 'x';
    }
    setFieldCellValue() {
        let column =
            prompt(
                `Сейчас ходят ${this.mode === 'x' ? 'крестики' : 'нолики'
                }. Пожалуйста введите, номер столбца, в котором хотите поставить крестик или нолик. Одно число от 1 до 3.`,
                1
            ) - 1;
        let row =
            prompt(
                `Сейчас ходят ${this.mode === 'x' ? 'крестики' : 'нолики'
                }. Пожалуйста, введите, номер строки, в котором хотите поставить крестик или нолик. Одно число от 1 до 3.`,
                1
            ) - 1;

        if (this.state[row][column] === null) {
            this.state[row][column] = this.mode;
        } else {
            alert('Эта клетка уже занята. Пожалуйста, выберите другую.');
            this.setFieldCellValue();
        }
    }
    printGameField() {
        // Выводит игровое поле.
        let result = '';
        for (const row of this.state) {
            result += `${row[0]} ${row[1]} ${row[2]} \n`;
        }
        alert(result);
    }
}

const gameField = new GameField();

while (!gameField.isOverGame) {
    gameField.setFieldCellValue();
    gameField.setMode();
    if (gameField.getGameFieldStatus() !== 'Игра ещё не окончена')
        gameField.isOverGame = true;
    gameField.printGameField();
}

alert(gameField.getGameFieldStatus());
